﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(sanji.Startup))]
namespace sanji
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
